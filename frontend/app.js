let getEl=(selector,parent=document)=>{
    return parent.querySelector(selector);
}
let getElAll=(selector,parent=document)=>{
    return [...parent.querySelectorAll(selector)];
}

getEl(".radiobuttons").addEventListener("click",(e)=>{
    if(e.target.name==="searchtype"){
        getEl(".errormessage").style.display="none";
        getElAll(".form").forEach((el)=>el.style.display="none");
        getEl(`.${e.target.value}`).style.display="block";
    }
})

let searchtype=()=>getElAll("[name='searchtype']").find(el=>el.checked).value;

let getResponse=(endpoint,jsondata)=>fetch(window.location.origin+"/"+endpoint,{method:"POST",headers:{'Content-Type': 'application/json'},body:JSON.stringify(jsondata)}).then(res=>{
    if(res.ok)return res.json(); 
    else throw res.text();
})


let fetchdata=(searchtype)=>{
    let apicase,requestjson;
    try{
            getEl(".errormessage").style.display="none";
            switch (searchtype){
                case "search_unique_episode":
                    let season=getEl("#season").value && Number(getEl("#season").value);
                    let episode=getEl("#episode").value && Number(getEl("#episode").value);
                    if(!season) throw "Please mention the season";
                    if(!episode) throw "Please mention the episode";
                    apicase="search_episode"
                    requestjson={season,episode};
                    break;
                case "search_multiple":
                    let blocks=[]; 
                    getElAll(".unit_block").forEach((el)=>{
                        let season=getEl(".season",el).value;
                        let episode=getEl(".episode",el).value;
                        if(!episode || !season)throw "Please fill all values.";
                        blocks.push({season,episode});
                    }) 
                    console.log(blocks);
                    apicase="search_multiple";
                    requestjson=blocks;
                    break;
                case  "search_name":    
                let name=getEl("#name").value;
                if(!name)throw "Please specify the name.";
                apicase="search_name";
                requestjson={name:name.toLowerCase()};    
            }
    }
    catch(err){
        console.log(err);
        let errorblock=getEl(".errormessage");
        errorblock.innerText=err;
        errorblock.style.display="block";
        return;
    }


    getResponse(apicase,requestjson).then(response=>{
        
        localStorage.setItem("response",JSON.stringify(response));

        window.location.href=window.location.origin+"/result.html";
        
    }).catch(err=>{

        err.then(info=>{
            console.log(info);
            let errorblock=getEl(".errormessage");
            errorblock.innerText=info;
            errorblock.style.display="block";
        });
    });

}

let currentindex=1;
let addmore=()=>{
        currentindex++

        let unit=document.createElement("div");

        unit.classList.add("unit_block");

        unit.innerHTML= `<div class="input-form">
        <label for="season_${currentindex}">Season</label>
        <input type="number" class="season" id="season_${currentindex}">
    </div>
    <div class="input-form">
        <label for="episode_${currentindex}">Episode</label>
        <input type="number" class="episode" id="episode_${currentindex}">
    </div>
    <div>
        <button onclick="deleteblock(event)">Delete</button>
    </div>`

        getEl(".search_params").appendChild(unit); 
}

let deleteblock=(e)=>{
    if(currentindex<=1)return;
    currentindex--;
    let block=e.target.parentElement.parentElement;
    block.parentElement.removeChild(block);
}