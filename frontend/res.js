
let resultstring=localStorage.getItem("response");

let result=resultstring && JSON.parse(resultstring);

console.log(result);

let episodeHTML=(data)=>{
    if(data)return ` <div class="episode">
        <picture>
            <source media="(min-width:992px)" srcset="${data.image && data.image.original}">
            <img src="${data.image && data.image.medium}" alt="No Image Found">
        </picture>
        <h2>${data.name}</h2>
        <div class="info"><b>Season:</b> ${data.season}</div>
        <div class="info"><b>Episode:</b> ${data.number}</div>
        <div class="info"><b>Runtime:</b> ${data.runtime}</div>
        <div class="summary info">
        <h4>Summary</h4>
        ${data.summary}
        </div>
    </div>`
}

if(result){

    let epiblock=document.createElement("div");
    epiblock.classList.add("list");
    let parent=document.querySelector("#maincontext");

    if(Array.isArray(result)){
        result.forEach(epi=>epiblock.innerHTML+=episodeHTML(epi));
    }
    else epiblock.innerHTML=episodeHTML(result);

    parent.appendChild(epiblock);

}

let goback=()=>{
    localStorage.removeItem("response");
    window.location.href=window.location.origin+"/";
}


