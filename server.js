const express=require("express");
const path=require("path");
const fs=require("fs");
const data=require("./data.json");


const server=express();

server.use(express.json());

server.use("/",express.static(path.join(__dirname,"frontend")));

server.use("/search_episode",(req,res,next)=>{
    let body=req.body;

    let final=body.season && body.episode && data._embedded.episodes.find(ep=>{
        return ep.season===body.season && ep.number===body.episode
    });
    res.locals.final=final;
    next();
})

server.use("/search_multiple",(req,res,next)=>{
    let body=req.body;
    let final=[];
    let minbody=body && body.map(({season,episode})=>season+","+episode);

    final=data._embedded.episodes.filter(ep=>minbody.includes(""+ep.season+","+ep.number));
    if(!final.length)final=null;
    res.locals.final=final;
    next();
});

server.use("/search_name",(req,res,next)=>{
    let body=req.body;
    
    let final=body.name && data._embedded.episodes.find(ep=>ep.name.toLowerCase().includes(body.name));
    res.locals.final=final;
    next();
})


server.use((req,res)=>{
    let final=res.locals.final;
    if(!final){
        res.setHeader("status",404);
        final="No Data Found!";
        return res.status(404).send(final).end();
    }
    res.status(200).json(final).end();
});


server.listen(3000,()=>{console.log("App is running on localhost:3000")});
